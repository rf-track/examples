function F = compute_force(B, SC)
    RF_Track;
    clight = RF_Track.clight;
    M = B.get_phase_space ('%X %Vx %Y %Vy %S %Vz %Q');
    X = M(:,1);
    Y = M(:,3);
    Z = M(:,5);
    Q = M(:,7);
    O = zeros(size(X));
    V = M(:,[ 2 4 6 ]);
    [E, B] = SC.get_field(X, Y, Z, O); % V/m
    F = Q .* (E + clight * cross(V, B)) ./ 1e6; % MeV/m
end
