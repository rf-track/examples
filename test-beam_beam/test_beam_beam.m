%%
%% CLIC 3 TeV beam-beam effects
%%

clear all
close all

RF_Track;

%% Simulation parameters
S0 = 0.2; % mm, initial distance from IP

%% init bunches
offsetY = 0.1 % nm, vertical offset at IP
M0e = init_electrons (-S0, 5000, -0.5*offsetY/1e6);
M0p = init_positrons ( S0, 5000, +0.5*offsetY/1e6);

B0p = Bunch6dT(M0p);
B0e = Bunch6dT(M0e);

%% main loop
D = 2*S0; % mm, simulation distance
D_step = 0.01; % mm, integration step
N_steps = D / D_step;
dt_mm = D_step; % mm/c, integration step

do_EE = 1; % simulate effect eletrons <- electrons
do_EP = 1; % simulate effect eletrons <- positrons
do_PP = 1; % simulate effect positrons <- positrons
do_PE = 1; % simulate effect positrons <- eletrons

do_plot = 1;

E_SY = [ mean(M0e(:,[ 5 3 ])) ];
P_SY = [ mean(M0p(:,[ 5 3 ])) ];

for i = 0:N_steps
    
    printf('Step %d/%d ...\r', i, N_steps)

    Fep = Fee = zeros(B0e.size(), 3);
    Fpe = Fpp = zeros(B0p.size(), 3);
    
    if do_EE || do_PE ; SC_ele = SpaceCharge_Field (B0e, 32, 32, 32); end
    if do_EP || do_PP ; SC_pos = SpaceCharge_Field (B0p, 32, 32, 32); end
    
    if do_EE ; Fee = compute_force (B0e, SC_ele); end
    if do_EP ; Fep = compute_force (B0e, SC_pos); end
    
    if do_PP ; Fpp = compute_force (B0p, SC_pos); end
    if do_PE ; Fpe = compute_force (B0p, SC_ele); end
    
    B0e.apply_force (Fee + Fep, dt_mm);
    B0p.apply_force (Fpe + Fpp, dt_mm);
    
    %% Plots
    if do_plot
        M0e = B0e.get_phase_space();
        M0p = B0p.get_phase_space();
        figure(1)
        clf ; hold on
        scatter (M0e(:,5), M0e(:,3)*1e6);
        scatter (M0p(:,5), M0p(:,3)*1e6);
        axis ([ -0.4 0.4 -15 15 ])
        xlabel ('S [mm]');
        ylabel ('Y [nm]');
        legend('electrons', 'positrons');
        grid
        drawnow
    end
end
