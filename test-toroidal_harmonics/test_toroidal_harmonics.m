RF_Track;

%% Load file
load Results_3.mat

%% Create RF-Track element
TH = ToroidalHarmonics(inp.R_major, inp.max_rminor);
TH.M00 = res.M00;
TH.set_Mcc(res.Mcc);
TH.set_Mcs(res.Mcs);
TH.set_Msc(res.Msc);
TH.set_Mss(res.Mss);

%% Map the field
Xa = linspace(-1.8, 1.8, 200) * 1e3; % mm
Ya = linspace(-1.8, 1.8, 201) * 1e3; % mm
Za = 100; % mm

Bx = [];
By = [];
Bz = [];
i=1;
for x = Xa
    j=1;
    for y = Ya
        [~,B] = TH.get_field(x, y, Za, 0);
        Bx(j,i) = B(1);
        By(j,i) = B(2);
        Bz(j,i) = B(3);
        j++;
    end
    i++;
end

%% Make the plots
figure(1)
Bx(abs(Bx)>10) = nan;
pcolor(Xa, Ya, Bx);
colorbar
shading flat
xlabel('X [mm]');
ylabel('Y [mm]');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('B_x [T]');

figure(2)
By(abs(By)>10) = nan;
pcolor(Xa, Ya, By);
colorbar
shading flat
xlabel('X [mm]');
ylabel('Y [mm]');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('B_y [T]');

figure(3)
Bz(abs(Bz)>10) = nan;
pcolor(Xa, Ya, Bz);
colorbar
shading flat
xlabel('X [mm]');
ylabel('Y [mm]');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
title('B_z [T]');

%%%% Validation
Rp = inp.R_major.*sqrt(1-(inp.max_rminor./inp.R_major).^2); 
k = @(eta) cosh(inp.xi_0)-cos(eta);
x_g = @(phi, eta) (Rp.*sinh(inp.xi_0).*cos(phi))./k(eta);
y_g = @(phi, eta) (Rp.*sinh(inp.xi_0).*sin(phi))./k(eta);
z_g = @(phi, eta) (Rp.*sin(eta)./k(eta)).* ones(size(k));

a = inp.a;
for iphi=1:400:length(inp.phi)
    phi = inp.phi(iphi);
    for ieta=1:10:length(inp.eta)
        eta = inp.eta(ieta);

        % Laura's input
        H_xi  = res.H_xi(iphi,ieta);
        H_eta = res.H_eta(iphi,ieta);
        H_phi = res.H_phi(iphi,ieta);
        H_x = ( H_xi.*cos(phi).*(1-cosh(inp.xi_0).*cos(eta))./k(eta))-(H_eta.*cos(phi).*sinh(inp.xi_0).*sin(eta)./k(eta))-sin(phi).*H_phi;
        H_y = ( H_xi.*sin(phi).*(1-cosh(inp.xi_0).*cos(eta))./k(eta))-(H_eta.*sin(phi).*sinh(inp.xi_0).*sin(eta)./k(eta))+cos(phi).*H_phi;
        H_z = (-H_xi.*sinh(inp.xi_0).*sin(eta)./k(eta))+(H_eta.*(cosh(inp.xi_0).*cos(eta)-1)./k(eta));
        H_REF = [ H_x H_y H_z ];
        
        % RF-Track's reconstruction
        x = x_g(phi,eta) * 1e3;
        y = y_g(phi,eta) * 1e3;
        z = z_g(phi,eta) * 1e3;
        [~,B] = TH.get_field(x, y, z, 0);
        H_RFT = B * 795774.715023846; % A/m, T/mu0 = 795774.715023846 A/m

        % difference
        printf('[ %g %g %g ]\n', x, y, z);
        printf('H_Ref = [ %g %g %g ]\n', H_REF);
        printf('H_RFT = [ %g %g %g ]\n\n', H_RFT);
    end
end