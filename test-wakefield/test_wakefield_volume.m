RF_Track;

%% define wakefield
SRWF = ShortRangeWakefield(0.002, 0.005, 0.006); % a,g,l

D = Drift(1);
D.add_collective_effect(SRWF);
D.set_cfx_nsteps(10);

%% bunch
sigma_z = 0.1; % mm
Pspread = 0.001; % permil, momentum spread
Pref = 50; % MeV/c
Twiss = Bunch6dT_twiss();
Twiss.sigma_z = sigma_z; % mm/c
Twiss.sigma_pt = Pspread; % permil

B0 = Bunch6dT(RF_Track.electronmass, RF_Track.nC, -1, Pref, Twiss, 1000);
M0 = B0.get_phase_space();

%% volume
V = Volume();
V.add(D, 0, 0, 0);

%% tracking options
TO = TrackingOptions();
TO.cfx_dt_mm = 100;

%% main loop
figure(1); clf ; hold on
figure(2); clf ; hold on
for X = linspace(-1, 1, 15)

    M0(:,1) = X;
    B0.set_phase_space(M0);
    
    %% track
    B1 = V.track(B0, TO);
    
    %% do plots
    M1 = B1.get_phase_space();
    
    figure(1)
    scatter3(M1(:,5), M1(:,1), M1(:,2))

    figure(2)
    scatter3(M1(:,5), M1(:,1), M1(:,6))
end

figure(1)
xlabel('S [mm]');
ylabel('x [mm]');
zlabel('Px [MeV/c]');

figure(2)
xlabel('S [mm]');
ylabel('x [mm]');
zlabel('Pz [MeV/c]');
