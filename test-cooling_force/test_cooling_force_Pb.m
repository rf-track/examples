%% Load RF_Track
RF_Track;

setenv("DEBUG"); % triggers RF-Track debug information

format long

%% Cooler %%
Cooler.L = 3; % m
Cooler.B = 0.070; % T
Cooler.r0 = 250; % mm, electron beam radius

%% Parameters range %%
Betas = 10.^linspace(4, 6, 40); % m/s

TT = zeros(size(Betas));
for i = 1:prod(size(TT))

    %% Ion beam
    N_particles = 10000; % number of macroparticles per simulated bunch

    Ions.A = 207.2; % Lead
    Ions.Q = 54; % ion charge
    Ions.mass = Ions.A * RF_Track.protonmass;
    Ions.Np = 1e10; % total number of ions per simulated bunch
    Ions.sigmaz = 1; %%%% 25 * pi * 1e3; % LEIR circumference mm
    Ions.K0 = 4.2 * Ions.A; % MeV
    Ions.E0 = Ions.mass + Ions.K0; % MeV
    Ions.P0 = sqrt(Ions.E0^2 - Ions.mass^2);
    Ions.beta0 = Ions.P0 / hypot(Ions.mass, Ions.P0); % c
    Ions.gamma0 = 1 / sqrt(1 - Ions.beta0^2); %

    Pc = Ions.P0 * (1.0 + 9.7e-3 * (0.5-rand(N_particles,1))); % MeV/c
    T(:,6) = Pc;
    B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, T);

    %% Electron beam parameters
    dbeta = Betas(i) / RF_Track.clight; % c
    beta = relativistic_velocity_addition([ 0 0 Ions.beta0 ], -[ 0 0 dbeta ] ); % c

    Electrons.beta = beta(3); % electron velocity, c
    Electrons.gamma = 1 / sqrt(1 - dot(Electrons.beta, Electrons.beta)); %
    Electrons.P = RF_Track.electronmass * Electrons.gamma * Electrons.beta;
    Electrons.Ne = 4e13; % electron number density #/m^3

    %% Cooler
    Nx = 16;
    Ny = 16;
    Nz = 16;
    E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
    E.set_Q(-1);
    E.set_electron_mesh(Nx, Ny, Nz, Electrons.Ne, 0, 0, Electrons.beta);
    E.set_static_Bfield(0.0, 0.0, Cooler.B);
    E.set_temperature(0.01, 0.001);

    %% Cooling simulation
    L = Lattice();
    L.append(E);
    L.track(B0);

    %% Load force from file and tabulate it
    F = load('cooling_force_beam.txt');

    TT(i) = -mean(F(:,3)) * 1e6; % eV/m

end

Fz_max = max(TT(end,:))

F =[ Betas(1,:)(:), TT(1,:)(:) ];
save -text data_Pb_uniform.dat F

figure(1)
clf
loglog(Betas, TT, 'r^;Simulated;', 'markersize', 8, 'markerfacecolor', 'r');
axis([ 5000 1e6 1 2e2 ]);
xlabel('V_{z,rel} [m/s]');
ylabel('F_z [eV/m]');
load EXP.mat
hold on
loglog(myVelocityStepAssumed_m_s, myForceS_eV_m, 'b*;Measured;')

F = [myVelocityStepAssumed_m_s(:) myForceS_eV_m(:) ];
save -text myForceS_eV_m.dat F

print -dpng plot.png
