%% Load RF_Track
RF_Track;

setenv("DEBUG"); % triggers RF-Track debug information

format long

%% Cooler %%
Cooler.L = 1; % m
Cooler.B = 0.1; % T
Cooler.r0 = 25; % mm, electron beam radius

%% Electron beam parameters %%
Electrons.beta = 0.6; % electron velocity, c
Electrons.gamma = 1 / sqrt(1 - dot(Electrons.beta, Electrons.beta)); %
Electrons.P = RF_Track.electronmass * Electrons.gamma * Electrons.beta;
Electrons.Ne = 1e12 * Electrons.gamma; % electron number density #/m^3 in the lab frame

Nx = 16;
Ny = 16;
Nz = 16;

E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
E.set_Q(-1);
E.set_electron_mesh(Nx, Ny, Nz, Electrons.Ne, 0, 0, Electrons.beta);
E.set_static_Bfield(0.0, 0.0, Cooler.B);
E.set_temperature(0.11, 0.1e-3);

%% Ion beam %%

Ions.A = 131.293; % Xenon
Ions.Q = 54; % ion charge
Ions.mass = Ions.A * RF_Track.protonmass;
Ions.Np = 5e8; % total number of ions per simulated bunch
Ions.sigmaz = 1; % bunch length, mm

[Betas, Xps]  = meshgrid(10.^linspace(2, 7, 40), linspace(0, 0.30, 40)); % m/s x mrad

Np = 1000;
Z = zeros(Np,1);

T = zeros(size(Betas));
for i = 1:prod(size(T))
    
    betaz = Betas(i) / RF_Track.clight; % c
    sigma_xp = Xps(i); % mrad
    yp = 0;      % mrad

    beta  = [ 0 0 betaz ];
    beta  = relativistic_velocity_addition([ 0 0 Electrons.beta ], beta ); % c
    gamma = 1 ./ sqrt(1 - dot(beta, beta)); %
    
    Xp = sigma_xp*randn(Np,1);
    
    P = Ions.mass * gamma * beta; % MeV/c
    Pz = P(3)*ones(Np,1);
    Px = Xp .* Pz / 1e3;
    Py = Z .* Pz / 1e3;
    
    B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, [ Z Xp Z Z Z hypot(Px,Py,Pz) ] );
    
    L = Lattice();
    L.append(E);
    L.track(B0);

    F = load('cooling_force_beam.txt');

    T(i) = -mean(F(:,3)) * 1e6; % eV/m

end

Fz_max = max(T(end,:))

figure(1)
loglog((Betas(end,:)), (T(end,:)), 'r^', 'markersize', 8, 'markerfacecolor', 'r');
axis([ 100 2e7 0.5e-3 10 ]);
xlabel('V_{z,rel} [m/s]');
ylabel('F_z [eV/m]');
print -dpng plot.png

figure(2)
surf(log10(Betas), Xps, log10(T));
xlabel('log10(V_{z,rel}) [m/s]');
ylabel('xp [mrad]');
zlabel('log10(F_z) [eV/m]');

