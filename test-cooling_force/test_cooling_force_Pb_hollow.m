%% Load RF_Track
RF_Track;

setenv("DEBUG"); % triggers RF-Track debug information

format long

%% Cooler %%
Cooler.L = 3; % m
Cooler.B = 0.070; % T
Cooler.r0 = 10; % mm, electron beam radius

%% Parameters range %%
[Betas, Emitts]  = meshgrid(10.**linspace(4, 6, 40), [ 7 7 ]); % m/s x mm.mrad

TT = zeros(size(Betas));
for i = 1:prod(size(TT))

    dbeta = Betas(i) / RF_Track.clight; % c
    emitt = Emitts(i); % mm.mrad
    
    %% Ion beam
    N_overlap = 10; % number of overlapping gaussians
    N_particles = 10000 / N_overlap; % number of macroparticles per overlap

    Ions.A = 207.2; % Lead
    Ions.Q = 54; % ion charge
    Ions.mass = Ions.A * RF_Track.protonmass;
    Ions.Np = 1e10; % total number of ions per simulated bunch
    Ions.sigmaz = 1; %%%% 25 * pi * 1e3; % LEIR circumference mm
    Ions.K0 = 4.2 * Ions.A; % MeV
    Ions.E0 = Ions.mass + Ions.K0; % MeV
    Ions.P0 = sqrt(Ions.E0**2 - Ions.mass**2);
    Ions.beta0 = Ions.P0 / hypot(Ions.mass, Ions.P0); % c
    Ions.gamma0 = 1 / sqrt(1 - Ions.beta0**2); %

    P_range = Ions.P0 * (1.0 .+ 9.7e-3 * linspace(-0.5, +0.5, N_overlap)); % MeV/c
    E_range = hypot(Ions.mass, P_range); % MeV
    V_range = P_range ./ E_range; % c

    T = [];
    Vz = [];
    for P = P_range
        Ions.P = P; % MeV/c
        Ions.E = hypot(Ions.mass, Ions.P); % MeV
        Ions.K = Ions.E - Ions.mass; % MeV
        Ions.beta = Ions.P / Ions.E;
        Ions.gamma = 1 / sqrt(1 - Ions.beta**2); %
        Ions.P_spread = 0.044; % 0.4 percent "uniform", obtained with 10 adjacent gaussians
        BunchT = Bunch6d_twiss();
        BunchT.emitt_x = Ions.beta * Ions.gamma * emitt; % mm.mrad == micron
        BunchT.emitt_y = Ions.beta * Ions.gamma * emitt; % mm.mrad == micron
        BunchT.sigma_t = Ions.sigmaz; % mm/c
        BunchT.sigma_pt = Ions.P_spread * 10; % permil
        BunchT.alpha_x = 1.5 / 5;
        BunchT.alpha_y = 1.5 / 5;
        BunchT.beta_x = 5 + 1.5**2 / 5; % m
        BunchT.beta_y = 5 + 1.5**2 / 5; % m
       
        B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, Ions.P BunchT, N_particles);
        T = [ T ; B0.get_phase_space() ];
        Vz = [ Vz ; B0.get_phase_space("%Vz") ];
    end

    Lcirc_mm = 25*pi*1e3; % mm
    T(:,5) = rand(N_particles*N_overlap, 1) * Lcirc_mm * 70 ./ Vz; % one injection takes = ~70 turns;
    
    B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, T);
    
    %% Electron beam parameters
    beta = relativistic_velocity_addition([ 0 0 Ions.beta0 ], -[ 0 0 dbeta ] ); % c

    Electrons.beta = beta(3); % electron velocity, c
    Electrons.gamma = 1 / sqrt(1 - dot(Electrons.beta, Electrons.beta)); %
    Electrons.P = RF_Track.electronmass * Electrons.gamma * Electrons.beta;
    Electrons.Ne = 4e13; % electron number density #/m^3
    
    Nx = 16;
    Ny = 16;
    Nz = 16;
    
    %% Cooler
    Rx = linspace(-Cooler.r0, Cooler.r0, Nx);
    Ry = linspace(-Cooler.r0, Cooler.r0, Ny);
    [X,Y] = meshgrid(Rx, Ry);
    R = hypot(X,Y);
    R_max = max(max(R));
    Electrons.Ne = Electrons.Ne .* ((R ./ R_max).^2);
    Electrons.Vx = zeros(Nx,Ny);
    Electrons.Vy = zeros(Nx,Ny);
    Electrons.Vz = Electrons.beta * ones(Nx,Ny);

    E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
    E.set_Q(-1);
    E.set_static_Bfield(0.0, 0.0, Cooler.B);
    E.set_electron_mesh(Nz, Electrons.Ne, ...
                            Electrons.Vx, ...
                            Electrons.Vy, ...
                            Electrons.Vz);
    E.set_temperature(0.01, 0.001);
    
    %% Cooling simulation
    
    E.track(B0);

    F = load('cooling_force_beam.txt');

    TT(i) = -mean(F(:,3)) * 1e6; % eV/m

end

Fz_max = max(TT(end,:))

F =[ Betas(1,:)(:), TT(1,:)(:) ];
save -text data_Pb_hollow.dat F

figure(1)
clf
loglog((Betas(1,:)), (TT(1,:)), 'r^;Simulated;', 'markersize', 8, 'markerfacecolor', 'r');
axis([ 5000 1e6 1 2e2 ]);
xlabel('V_{z,rel} [m/s]');
ylabel('F_z [eV/m]');
load EXP.mat
hold on
loglog(myVelocityStepAssumed_m_s, myForceS_eV_m, 'b*')

print -dpng plot.png

figure(2)
clf
loglog((Betas(2,:)), (TT(2,:)), 'r^', 'markersize', 8, 'markerfacecolor', 'r');
axis([ 5000 1e6 1 2e2 ]);
xlabel('V_{z,rel} [m/s]');
ylabel('F_z [eV/m]');
load EXP.mat
hold on
loglog(myVelocityStepAssumed_m_s, myForceS_eV_m, 'b*')
print -dpng plot.png

%{
figure(3)
surf(log10(Betas(1,:)), [ 7 14 ], log10(T));
xlabel('log10(V_{z,rel}) [m/s]');
ylabel('xp [mrad]');
zlabel('log10(F_z) [eV/m]');
%}

% plot current density
figure(4)
C = zeros(size(Electrons.Ne));
for i=1:size(C,1)
    for j=1:size(C,2)
        C(i,j) = E.get_current_density(Rx(i), Ry(j), 1.0)(3) / 10000; % A/cm**2
    end
end
surf(Rx, Ry, C);
