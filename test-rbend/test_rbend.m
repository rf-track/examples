RF_Track;

%% 1 GeV proton
m = RF_Track.protonmass;
P = 1000; % MeV/c
Q = +1;
B0 = Bunch6dT([ 0 0 0 0 0 P m Q ]);

%% drift with By != 0.0
rho = 10; % m, bending radius
By = (P/Q) * 1e6 / rho  / RF_Track.clight; % T
Brho = By * rho; % T*m

D = Drift(0.5);
D.set_static_Bfield(0, By, 0);

%% 3D tracking
V = Volume();
V.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'rk8pd'
V.odeint_epsabs = 1e-6;
V.dt_mm = 1; % mm/c
V.open_boundaries = false;
V.add(D, 0, 0, 0);

V.track(B0);
B1 = V.get_bunch_at_s1();
M1_fieldMap = B1.get_phase_space()

%% SBend map
B0 = Bunch6d([ 0 0 0 0 0 P m Q ]);

B = SBend(0.5);
B.set_Brho(Brho);
B.set_Bfield(By);
B.set_h(0.0);
Angle = B.get_angle();
B.set_E1(Angle/2);
B.set_E2(Angle/2);

L = Lattice();
L.append(B);

B1 = L.track(B0);
M1_Sbend = B1.get_phase_space()

%% RBend map
R = RBend(0.5 / 1.000104174262653, Angle, P/Q);
R.set_h(0.0); % make the reference frame straight

L = Lattice();
L.append(R);

B1 = L.track(B0);
M1_Rbend = B1.get_phase_space()
