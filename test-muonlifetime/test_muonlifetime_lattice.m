RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.muonmass; % MeV/c^2
Part.P = 1000; % MeV/c
Part.Q = -1; % e

%% Bunch
N = 10000;
X = randn (N,1);
Y = randn (N,1);
O = zeros (N,1);
I = ones  (N,1);

B0 = Bunch6d(Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);
B0.set_lifetime (RF_Track.muonlifetime);

%% Lattice
D = Drift (20e3); % 20 km
D.set_tt_nsteps (1000); % transport table, tracks average quantities in 1000 steps

L = Lattice ();
L.append (D);

tic
B1 = L.track (B0);
toc

T = L.get_transport_table ('%S %N'); % transmission vs S

plot (T(:,1) / 1e3, T(:,2));
xlabel ('S [km]');
ylabel ('transmission [%]');

