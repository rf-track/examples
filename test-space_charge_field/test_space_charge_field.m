RF_Track;

N_electrons = 1e6; % e, number of particles in the distribution
Q = -1; % e, charge of one electron

N = 100000; % number of simulated macro particles

%% Fill a sphere uniformly
R = qrand(N,3); % quasi-random numbers
radius = 1; % mm, radius of the sphere
rvals = 2*R(:,1)-1;
elevation = asin(rvals); % rad
azimuth = 2*pi*R(:,2); % rad
radii = radius*R(:,3).^(1/3); % mm
[X, Y, Z] = sph2cart(azimuth, elevation, radii);

%% Create a beam
O = zeros(N,1);
B = [ X O Y O Z O ];
B0 = Bunch6dT (RF_Track.electronmass, N_electrons, Q, B);

figure(1)
clf
hold on
LGND = '';

for NN = [ 32 48 64 ] 
    Nx = NN; 
    Ny = NN;
    Nz = NN;

    SC = SpaceCharge_Field(B0, Nx, Ny, Nz, 4);

    X = linspace(-10, 10, 1000); % mm
    [E,B] = SC.get_field(X, 0, 0, 0);
    
    plot(X, E(:,1)/1e3, 'linewidth', 2);
    LGND = [ LGND ; sprintf('PIC %dx%dx%d  ', Nx, Ny, Nz) ];
    
    drawnow;
end

%% Analytic formula
Er = [];
Ra = linspace(-10, 10, 1000); % mm
for r = Ra
    if abs(r) < radius
        % e / 4 pi epsilon0 mm^2 = 0.0014399645 V/m
        E = 0.0014399645 * Q * N_electrons * r / radius^3; % V/m
    else
        % e / 4 pi epsilon0 mm^2 = 0.0014399645 V/m
        E = 0.0014399645 * Q * N_electrons * r / abs(r)^3; % V/m
    end
    Er = [ Er ; E ];
end

%% Plots
plot(Ra, Er/1e3,'linewidth',2);
title('Radial electric field of a uniformly charged sphere');
xlabel('r [mm]')
ylabel('E_r [kV/m]')
LGND = [ LGND ; 'Analytic formula' ];
legend(cellstr(LGND));
grid on;
print -dpng plot_Er.png
