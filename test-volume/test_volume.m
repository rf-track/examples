setenv("DEBUG");

RF_Track;

V = Volume();
D = Drift(1.2);

% Should expect that respectively entrance, center, and exit are at
% 0, 0, 1000 mm in each respective case

disp('');
disp('REFERENCE = entrance');
V.add(D, 0, 0, 1, 0, 0, 0)

disp("ROLL = ");
V.add(D, 0, 0, 1, pi/100, 0, 0)

disp("PITCH = ");
V.add(D, 0, 0, 1, 0, pi/100, 0)

disp("YAW = ");
V.add(D, 0, 0, 1, 0, 0, pi/100)

disp('');
disp('REFERENCE = exit');
V.add(D, 0, 0, 1, 0, 0, 0, 'exit')

disp("ROLL = ");
V.add(D, 0, 0, 1, pi/2, 0, 0, 'exit')

disp("PITCH = ");
V.add(D, 0, 0, 1, 0, pi/2, 0, 'exit')

disp("YAW = ");
V.add(D, 0, 0, 1, 0, 0, pi/2, 'exit')

disp('');
disp('REFERENCE = center');
V.add(D, 0, 0, 1, 0, 0, 0, 'center')

disp("ROLL = ");
V.add(D, 0, 0, 1, pi/2, 0, 0, 'center')

disp("PITCH = ");
V.add(D, 0, 0, 1, 0, pi/2, 0, 'center')

disp("YAW = ");
V.add(D, 0, 0, 1, 0, 0, pi/2, 'center')
