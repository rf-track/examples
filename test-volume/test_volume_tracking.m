RF_Track;

%% beam, 100 MeV/c proton
Pz = 1000; % MeV/c
B0 = Bunch6dT([ 0 0 0 0 0 Pz RF_Track.protonmass +1 1 0.0 ]);

%% A dipole magnet, that is, a drift with By

rho = 1; % m, bending radius
By = Pz * 1e6 / rho  / RF_Track.clight; % T

Dp = Drift(1.5);
Dp.set_static_Bfield(0, By, 0);

clf
hold on
for phi = linspace(0, 2*pi, 17)

    %% Volume
    V = Volume();
    V.add(Dp, 0, 0, 1, phi, 0, 0); % rotated the dipole by an angle phi
    V.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'rk8pd'
    V.odeint_epsabs = 1e-6; % absolute error
    V.dt_mm = 10; % mm/c, integration step
    V.tt_dt_mm = 100; % tabulation step, tabulate average quantities
    
    B1 = V.track(B0);
    T = V.get_transport_table('%mean_X %mean_Y %mean_Z');
    
    % plots
    plot3(-T(:,1), T(:,3), T(:,2));
    drawnow;
    
end