RF_Track;

N_particles = 10000; % number of macroparticles per overlap

Ions.A = 207.2; % Lead
Ions.Q = 54; % ion charge
Ions.mass = Ions.A * RF_Track.protonmass;
Ions.Np = 5e8; % total number of ions per simulated bunch
Ions.sigma_t = 1; % mm/c
Ions.K = 4.2 * Ions.A; % MeV
Ions.E = Ions.mass + Ions.K; % MeV
Ions.P = sqrt(Ions.E^2 - Ions.mass^2);
Ions.beta_gamma = Ions.P / Ions.mass; %
Ions.P_spread = 0.01; % 

BunchT = Bunch6d_twiss();
BunchT.emitt_x = Ions.beta_gamma * 14; % mm.mrad == micron
BunchT.emitt_y = Ions.beta_gamma * 14; % mm.mrad == micron
BunchT.sigma_t = Ions.sigma_t; % mm/c
BunchT.sigma_pt = Ions.P_spread * 10; % permil

BunchT.alpha_x = 0;
BunchT.alpha_y = 0;
BunchT.beta_x = 3.690910796; % m
BunchT.beta_y = 14.3080807; % m

B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, Ions.P, BunchT, N_particles, -1);

load ('twiss_noEC.txt'); % this is T
RING = T;

% chromaticity
DQx = -0.5; % * 2pi
DQy = -1.0; % * 2pi

% momentum compaction
LEIR_momentum_compaction = 0.124;

RING(1,2) = RING(end,2) = 3.690910796;
RING(1,3) = RING(end,3) = 0.0;
RING(1,5) = RING(end,5) = 14.3080807;
RING(1,6) = RING(end,6) = 0.0;

%MR = TransferLine('twiss_noEC.tfs', Ions.P);
MR = TransferLine(RING, DQx, DQy, LEIR_momentum_compaction, Ions.P);
MR.set_nsteps(100);
MR.set_tt_nsteps(100);

L = Lattice(); % Folds Nfold turns in one lattice
L.append(MR);
B1 = L.track(B0);

T = L.get_transport_table("%S %beta_x %beta_y");

figure(1)
clf
plot(RING(:,1), RING(:,2))
hold on
plot(T(:,1), T(:,2));
xlabel('S [m]')
ylabel('beta_x [m]')
legend('MAD-X Twiss', 'Tracking');

figure(2)
clf
plot(RING(:,1), RING(:,5))
hold on
plot(T(:,1), T(:,3));
xlabel('S [m]')
ylabel('beta_y [m]')
legend('MAD-X Twiss', 'Tracking');

