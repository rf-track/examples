clear all;
close all; clc; 

% Load RF_Track
RF_Track;

% Structure parameters

Ncells        = 34; 
freq          = 12; % GHz
phase_advance = pi/2;
vg            = 0.453; % c
r_Q           = 2300; % Ohm/m
Q             = 7200; 

% BL module
tic
BL = BeamLoading (Ncells, freq*1e9, phase_advance, Q, r_Q, vg);
toc

w = BL.get_wake_function(); % V/pC/m
hz = BL.get_dt(); % mm/c
Lcell = BL.get_Lcell(); % m

Ns = length(w); 
s_axis = linspace(0, (Ns -1)*hz, Ns); % mm 

f1 = figure(1);
plot(s_axis, w, 'LineWidth', 2); 
xlabel('s = c\Delta{t} [mm]'); ylabel('w_l [V/pC/m]'); 
set(gca,'FontSize', 15); 


%% Define Beam
% Define bunch
Nparticles = 1000;
Part.mass  = RF_Track.electronmass; % MeV/c2
Part.Q     = -1; % e
Part.P     = 2400; % MeV
Psigma     = 0.00 * Part.P; % MeV/c
beta_gamma = Part.P / Part.mass; 
aver_I     = 100; % A
fb         = 11.994e9; % Hz
population = aver_I * RF_Track.C / fb; % e - 1 A/Hz = * 6.2415091e+18 e

Twiss          = Bunch6d_twiss();
Twiss.emitt_x  = 100; % mm.mrad normalised emittance
Twiss.emitt_y  = 100; % mm.mrad
Twiss.beta_x   = 1.25; % m
Twiss.beta_y   = 1.25; % m
Twiss.alpha_x  = 0;
Twiss.alpha_y  = 0;
Twiss.sigma_t  = 1; % mm/c, longitudinal dispersion
Twiss.sigma_pt = 0; % permil

B0 = Bunch6d_QR(Part.mass, population, Part.Q, Part.P, Twiss, Nparticles, -1);

% Define beam
Nbunches = 241*12; 
spacing = RF_Track.s / fb; % mm/c
B = Beam(15, B0, spacing); % Nbunches = 20 for faster computation

%% TRACKING 
PETS = TimeDependent_Field ( Ncells * Lcell );
PETS.add_collective_effect(BL);
PETS.set_cfx_nsteps(Ncells);

%% Lattice - Concatenation of several PETS
Npets = 10; 
L = Lattice();
for s = 1:Npets  
    L.append(PETS);
end

% Autophase
P0 = Bunch6d(RF_Track.electronmass, population, -1, [ 0 0 0 0 0 2400 ] );
max_energy = L.autophase(P0)
fprintf('Autophase finished \n'); 

% perform tracking
tic
B1 = L.track(B);
toc 

c = RF_Track.clight / 1e6; % mm/ns - speed of light

figure(2)

for bunch = 1:15
    M1 = B1{bunch}.get_phase_space();
    M0 = B{bunch}.get_phase_space();  
    scatter(M1(:,5) / c, M1(:,6),'r')
    hold on
    scatter(M1(:,5) / c, M0(:,6),'b')
endfor

str_title = sprintf('Beam Energy after %5.2f PETS', Npets); 
title(str_title,'FontSize',15)
grid on 
xlabel('t [ns]','FontSize',15)
ylabel('E [MeV]', 'FontSize', 15) 
h = legend('Initial Energy','Final Energy','Location','NorthEast');
set(h, 'FontSize',15)
set(gca, 'FontSize',12,'LineWidth',1)

figure(3)

for bunch = 15
    M1 = B1{bunch}.get_phase_space();
    M0 = B{bunch}.get_phase_space();  
    scatter(M1(:,5) / c, M1(:,6),'r')
    hold on
    scatter(M1(:,5) / c, M0(:,6),'b')
endfor

str_title = sprintf('Bunch Energy after %5.2f PETS', Npets); 
title(str_title,'FontSize',15);
grid on 
xlabel('t [ns]','FontSize',15);
ylabel('E [MeV]', 'FontSize', 15);
h = legend('Initial Energy','Final Energy','Location','NorthEast');
set(h, 'FontSize',15);
set(gca, 'FontSize',15,'LineWidth',1);
