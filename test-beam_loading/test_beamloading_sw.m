clear all; close all; clc;

%% Load RF-Track
RF_Track;
c = RF_Track.clight*1e-6; % mm/ns 

%% Load data
display('Loading data and setting-up gun ...') 
Edata = load('GUN_3GHz_2.6_CELL_AXIS.SF7');  
z         = Edata(:,1) * 1000; % mm                                        
Ez        = Edata(:,2); % V/m
dz        = z(2) - z(1); % mm

%% Input parameters for RF-field constructor
max_Ez = 80e6; % V/m
Ez     = Ez / max(Ez) * max_Ez; % V/m                                          
freq   = 2.997; % GHz                                               
w      = 2 * pi * freq / c; % c/mm
Lcell  = pi / w; % mm
Ncells = 2.6; 
Ltotal = range(z); % mm

initialphid = -69.0; % deg
initialt0   = 0.0; % mm/c

%% RF-Fieldmap
Gun  = RF_FieldMap_1d_CINT(Ez, dz/1000, Ltotal/1000, freq*1e9, +1);
Gun.set_t0(initialt0);
Gun.set_phid(initialphid);

%% Structure data for BL constructor
Q_factor = 14500; % Unloaded Q-factor                                                     
Rshunt   = 50e6; % Ohm/m                                         
r_Q      = Rshunt / Q_factor; % Ohm/m

%% BL constructor
display('Loading BeamLoadingSW ... ') 

injection_tau = 10; % units of tau, attenuation time of the SW structure (2Q/omega)

tic
BL = BeamLoadingSW(Gun, Q_factor, r_Q, Ncells, RF_Track.electronmass, -1, injection_tau);
toc
tinj = BL.get_tinj; % mm/c
Gun.add_collective_effect(BL);

% Generator - photocathode
Q_pC = 300; % pC
G = Bunch6dT_Generator();
G.species = "electrons";          % species
G.cathode = true;                 % cathode, true or false
G.noise_reduc = true;             % noise reduction (quasi-random)
G.q_total = Q_pC / 1000;          % nC bunch charge
G.ref_ekin = 0;                   % MeV energy of reference particle
G.ref_zpos = 0;                   % m position of reference particle
G.ref_clock = 0;                  % ns clock of reference particle
G.phi_eff = 3.5;                  % eV, effective work function
G.e_photon = 4.73;                % eV, photon energy for Fermi-Dirac distribution.
G.dist_x = 'g';                   % Specifies the transverse particle distribution in the horizontal direction.
G.dist_y = 'g';                   % Specifies the transverse particle distribution in the vertical direction.
G.dist_z = 'g';                   % Specifies the longitudinal particle distribution
G.sig_x = 1.657;                  % mm, rms bunch size in the horizontal direction. Also the vertical bunch size if Dist_x = radial.
G.sig_y = 1.657;                  % mm, rms bunch size in the vertical direction.
G.sig_t = 0.002;                  % ns, rms value of the emission time, i.e. the bunch length if generated from a cathode
G.c_sig_x = 5;                    % cuts off a Gaussian horizontal distribution
G.c_sig_y = 5;                    % cuts off a Gaussian vertical distribution
G.c_sig_t = 5;                    % cuts off a Gaussian longitudinal distribution
G.dist_pz = 'fd_300';             % Emission from cathode following a Fermi-Dirac dist. (for z)

%% Create bunch
Nparticles = 1000;
B0 = Bunch6d(G, Nparticles);

%% Plot initial bunch
M0 = B0.get_phase_space();
 
figure(1);
scatter(M0(:,5), M0(:,6))
xlabel('s [mm]'); 
ylabel('Pc [MeV]'); 
title('Bunch at cathode')
set(gca, 'FontSize', 15)

%% Define Beam
display('Defining train of particles ... ')
Nbunches = 50; 
fb       = freq / 2.0 * 1e9; % Hz
spacing  = RF_Track.s / fb; % mm/c

B = Beam(Nbunches, B0, spacing); 

%% Volume
figure(2) ; clf ; hold on
figure(3) ; clf ; hold on
for nsteps = [ 50 100 ]
    V = Volume();
    V.add(Gun, 0, 0, 0);

    dt = RF_Track.s * (1 / fb) / nsteps; % mm/c

    % O = TrackingOptions();
    V.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    V.odeint_epsabs = 1e-6; % required accuracy
    V.dt_mm = 1*dt; % mm/c, integration step size
    V.cfx_dt_mm = 1*dt; % mm/c, apply Beam Loading effect every cfx_dt_mm
    
    L = Lattice(); 
    L.append(V); 
    
    display('Tracking under Beam Loading effect... '); 
    tic
    B1 = L.track(B);
    toc
				
    % Plot phase space
    figure(2);
    for bunch = 1:Nbunches
        M1 = B1{bunch}.get_phase_space();
        scatter(M1(:,5) , M1(:,6))
    end

    str_title = sprintf('Beam Energy after photoinjector \n');
    title(str_title,'FontSize',15)
    grid on
    xlabel('t [mm/c]','FontSize',15)
    ylabel('E [MeV]', 'FontSize', 15)
    axis([0 1e4 6 7]);
    set(gca, 'FontSize',15,'LineWidth', 1);
    drawnow
    
    figure(3);
    for bunch = Nbunches
        M1 = B1{bunch}.get_phase_space();
        scatter(M1(:,5), M1(:,6))
    end

    str_title = sprintf('Beam Energy after photoinjector for bunch = %d \n', bunch);
    title(str_title,'FontSize',15)
    grid on
    xlabel('t [mm/c]','FontSize',15)
    ylabel('E [MeV]', 'FontSize', 15)
    set(gca, 'FontSize',15, 'LineWidth', 1);
    drawnow

end
