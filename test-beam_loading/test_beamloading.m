clear all; close all; clc
RF_Track;
c = RF_Track.clight / 1e6; % mm/ns

% Test Beam Loading - Provide fieldmap
% Example based on CLIC's main linac. See https://clicdp.web.cern.ch/content/conceptual-design-report

load efield.dat.gz;

Ez      = squeeze(Ez(1,1,:)); % V/m - On axis field
N       = length(Ez);
L       = (N - 1) * hz; % m - Total Length
z_field = linspace(0, L * 1000, N); % mm
freq    = 11.9934 * 1e9; % Hz
Pmap    = 4; % W - Fieldmap with which the given field has been created
Pactual = 61.3e6; % W 

%% Figure 1: Understand fieldmap
plot(z_field, abs(Ez)/1e6)
xlabel('z [mm]'); ylabel(' |E_z| [MV/m]')
title('Ez seen by an ultrarelativistic particle')
set(gca,'FontSize',15)

% RF-element
TWS  = RF_FieldMap_1d_CINT(Ez, hz, L, freq, +1);
TWS.set_odeint_algorithm("rk2");
TWS.set_P_map( Pmap ); 
TWS.set_P_actual( Pactual ); 

TWS2  = RF_FieldMap_1d_CINT(Ez, hz, L, freq, +1);
TWS2.set_odeint_algorithm("rk2");
TWS2.set_P_map( Pmap ); 
TWS2.set_P_actual( Pactual ); 

% Beam-Loading required arguments
VG           = [1.65, 1.2, 0.83]/100; % c 
QQ           = [5536, 5635, 5738]; 
R_Q          = [14587 16220 17954]; % Ohm/m 
phaseadvance = 2*pi/3; % rad
Ncells       = 27; 

% Bunch specification - Steady state 
charge          = 600e-12; % C
particles_bunch = charge*RF_Track.C; % e
Nbunches        = 312; 
fb              = freq / 6; % Hz - Frequency of repetition of the bunches

%% A) STEADY-STATE CONSTRUCTOR
tic
BL_steady = BeamLoading(Ncells, freq, phaseadvance, QQ, R_Q, VG, -1, particles_bunch, fb);
toc

% Some information
G_steady = BL_steady.get_G_steady() / 1e6; % MV/m 
N        = length(G_steady); 
z0       = BL_steady.get_z0; 
z1       = BL_steady.get_z1;
zplot    = linspace(z0,z1,N); 

figure(2)
plot(zplot, G_steady,'LineWidth', 2)
xlabel('z [mm]'); ylabel(' G [MV/m]')
title('Beam induced field (STEADY)')
set(gca,'FontSize',15)

% B) TRANSIENT STATE CONSTRUCTOR 
tic
BL = BeamLoading(Ncells, freq, phaseadvance, QQ, R_Q, VG);
toc 

% Information - Calculate transient gradient for trains with equal bunches
BL.solve_pde_transient(-1 * particles_bunch, fb, Nbunches); 

G           = -BL.get_G() / 1e6; % MV/m
[N,M]       = size(G);
Lcell       = BL.get_Lcell * 1000; % mm
dt          = BL.get_dt; % mm/c -- by default, dt is always Lcell/10
last_t      = (M-1)*dt; % mm/c
t_transient = linspace(0,last_t, M); % mm/c 

tplot  = [0:15:75] * RF_Track.ns; % mm/c
indexx = ceil(tplot /dt) +1; 

f3 = figure(3); 
plot(zplot,G(:,1),'b','LineWidth',3) % unloaded
for s = indexx(2:end-1)
    hold on
    plot(zplot, G(:,s),'--r','LineWidth',2) 
endfor
plot(zplot, G(:,end), 'r', 'LineWidth',3)
grid on

xlabel('z [mm]'); ylabel(' G [MV/m]'); 
title('Beam induced field (TRANSIENT)')
h = legend('t = 0.0', 't = 15 ns', 't = 30 ns', 't = 45 ns', 't = 60 ns',  't = 75 ns', 'Location', 'SouthWest' );
set(gca, 'FontSize', 15);
set(h, 'FontSize',15)

%% TRACKING	
% Define bunch
Nparticles = 1000;
Part.mass = RF_Track.electronmass; % MeV/c2
Part.Q = -1; % e
Part.P = 9e3; % MeV (9 GeV)
Psigma = 0*15; % permil, relative energy spread
population = 3.7e9; % e

Twiss = Bunch6d_twiss();
Twiss.emitt_x = 600e-3; % mm.mrad normalised emittance
Twiss.emitt_y = 10e-3; % mm.mrad
Twiss.beta_x = 10; % m
Twiss.beta_y = 10; % m
Twiss.alpha_x = 0;
Twiss.alpha_y = 0;
Twiss.sigma_t = 0.045; % mm/c
Twiss.sigma_pt = Psigma; % permil, relative energy spread

B0 = Bunch6d(Part.mass, population, Part.Q, Part.P, Twiss, Nparticles, -1);
M0 = B0.get_phase_space();

% Shift bunch
tinj = 149 / fb; % s 
tinj = tinj * 2.9979246e+11; % mm/c

t  = M0(:,5); % mm/c
M0(:,5) = t + tinj; % mm/c
newB0 = Bunch6d(Part.mass, population, Part.Q, M0); 

% Lattice
L = Lattice(); % Lattice with transient BL effect
TWS.add_collective_effect(BL);
TWS.set_cfx_nsteps(Ncells);
L.append(TWS);

L2 = Lattice(); % Lattice with steady BL effect
TWS2.add_collective_effect(BL_steady); 
TWS2.set_cfx_nsteps(Ncells); 
L2.append(TWS2); 

%% Set optimal phase for TW structure
% Define witness bunch 
P0 = Bunch6d(RF_Track.electronmass, 1, -1, [ 0 0 0 0 0 9e3 ] );
max_energy = L.autophase(P0);

% Track
B1 = L.track(B0); % If one bunch is tracked in a Lattice with BL effect, it will only be affected by its short-range wakefield
M1 = B1.get_phase_space(); 

x  = M1(:,1); % mm
xp = M1(:,2); % mrad
y  = M1(:,3); % mm
yp = M1(:,4); % mrad
t  = M1(:,5); % mm/c
P  = M1(:,6); % MeV/c
E1 = hypot(P,RF_Track.electronmass); % MeV

B150 = L2.track(newB0); % If one bunch is tracked in a Lattice with steady BL effect, it will feel its short-range wakefield + BL steady state
M150 = B150.get_phase_space(); 

x150  = M150(:,1); % mm 
xp150 = M150(:,2); % mm
y150  = M150(:,3); % mm
yp150 = M150(:,4); % mm
t150  = M150(:,5); % mm
P150  = M150(:,6); % mm
E150  = hypot(P150, RF_Track.electronmass); % MeV

% Figure 4 - Energy comparison
figure(4)
E_hist = linspace(9015, 9040);
hist(E1,E_hist,'EdgeColor','none', 'FaceColor','b')
hold on
hist(E150,E_hist,'EdgeColor','none', 'FaceColor', 'r')
xlabel('E [MeV]');
legend('First bunch Energy', 'Final bunch Energy')
set(gca,'FontSize',15)

% Tracking and field accuracy
Espread_tracking = mean(E1) - mean(E150); % MeV

fprintf('BL Energy spread from tracking = %5.2f MeV \n', Espread_tracking); 

%% Train
% We consider now the problem of tracking a Beam() in a Lattice with transient BL.

spacing = RF_Track.s / fb; % mm/c
B = Beam(Nbunches, B0, spacing); % Nbunches = 20 for faster computation

tic
B1 = L.track(B); 
toc

f5 = figure(5); 
% Plots
for bunch = 1:Nbunches
    M1 = B1{bunch}.get_phase_space();
    M0 = B{bunch}.get_phase_space();  
    scatter(M1(:,5) / c, M1(:,6),'r')
    hold on
    scatter(M1(:,5) / c, M0(:,6),'b')
endfor

str_title = sprintf('Beam Energy after CLIC AS'); 
title(str_title,'FontSize',15)
grid on 
xlabel('t [ns]','FontSize',15)
ylabel('E [MeV]', 'FontSize', 15) 
h = legend('Final Energy','Initial Energy','Location','NorthEast');
set(h, 'FontSize',15)
set(gca, 'FontSize',12,'LineWidth',1)
