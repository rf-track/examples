clear all;
close all; clc;

RF_Track;
c = RF_Track.clight / 1e6; % mm/ns

% Test Beam Loading - Provide fieldmap
% Example based on CLIC's main linac. See https://clicdp.web.cern.ch/content/conceptual-design-report

load efield.dat.gz;

Ez      = squeeze(Ez(1,1,:)); % V/m - On axis field
N       = length(Ez);
L       = (N - 1) * hz; % m - Total Length
z_field = linspace(0, L * 1000, N); % mm
freq    = 11.9934 * 1e9; % Hz
Pmap    = 4; % W - Fieldmap with which the given field has been created
Pactual = 61.3e6; % W 

% RF-element
TWS  = RF_FieldMap_1d_CINT(Ez, hz, L, freq, +1);
TWS.set_odeint_algorithm("rk2");
TWS.set_P_map( Pmap ); 
TWS.set_P_actual( Pactual ); 

% Beam-Loading required arguments
VG           = [1.65, 1.2, 0.83] / 100; % c 
QQ           = [5536, 5635, 5738]; 
R_Q          = [14587 16220 17954]; % Ohm/m 
phaseadvance = 2*pi/3; % rad
Ncells       = 27; 

% Beam Loading Module
fprintf('Computing BL module ... \n'); 
tic
BL = BeamLoading(Ncells, freq, phaseadvance, QQ, R_Q, VG);
toc 

% Compensation
tpower          = linspace(0, 89); % ns 
dt_power        = tpower(2) * RF_Track.ns; % mm/c 
N_power         = length(tpower); 
Pinput          = Pactual * [linspace(0, 1, N_power), ones(1, N_power)]; %W 
injection_delay = 89 * RF_Track.ns; % mm/c

BL.set_unloaded_gradient(Pinput', dt_power, injection_delay, TWS); % To compensate BL in TW structures, we have to play with the initial gradient 

G_unloaded = BL.get_G_unloaded() / 1e6; % MV/m
[N_unl, M_unl] = size(G_unloaded);

% Beam-induced gradient precomputation
charge          = 600e-12; % C
particles_bunch = charge*RF_Track.C; % e
Nbunches        = 312; 
fb              = freq / 6; % Hz - Frequency of repetition of the bunches

BL.solve_pde_transient(-1 * particles_bunch, fb, Nbunches); 

G           = -BL.get_G() / 1e6; % MV/m
[N,M]       = size(G);
Lcell       = BL.get_Lcell * 1000; % mm
dt          = BL.get_dt; % mm/c
last_t      = (M-1) * dt; % mm/c
t_transient = linspace(0,last_t, M); % mm/c 

z0    = BL.get_z0; 
z1    = BL.get_z1;
zplot = linspace(z0, z1, N); 

% PLOTS

figure(1) 
fprintf('Plot power and gradients ... \n'); 

subplot(221)
x  = linspace(0, 2*N_power* dt_power - dt_power, 2*N_power); % mm/c
y1 = Pinput / 1e6; % MW
Iav = charge * fb; 
y2 = [zeros(1, N_power), Iav * ones(1, 100)]; % A 

[ax, h1, h2] = plotyy(x / RF_Track.ns, y1, x / RF_Track.ns, y2);
set(h1, 'LineWidth', 2);
set(h2, 'LineWidth', 2);
xlabel('t [ns]')
ylabel(ax(1), 'P [MW]')
ylabel(ax(2), 'I_a_v [A]')
title('Power inputs')
set(ax(1),'FontSize',15)
set(ax(2),'FontSize',15)

subplot(222)
tplot = [0:15:75] * RF_Track.ns; % mm/c
indexx = ceil(tplot / dt) + 1; 
plot(zplot, G_unloaded(:,1),'b','LineWidth',3) % unloaded
for s = indexx(2:end-1)
    hold on
    plot(zplot, G_unloaded(:,s),'--r','LineWidth',2) 
endfor
plot(zplot, G_unloaded(:,indexx(end)), 'r', 'LineWidth',3)
grid on
xlabel('z [mm]', 'FontSize', 15)
ylabel(' G [MV/m]', 'FontSize', 15)
title('Unloaded gradient' , 'FontSize', 15)
h = legend('t = 0.0', 't = 15 ns', 't = 30 ns', 't = 45 ns', 't = 60 ns',  't = 75 ns', 'Location', 'NorthWest' );
set(h, 'FontSize',15)
set(gca, "linewidth", 1, "Fontsize", 15);

subplot(223)
i = 1; 
tplot = [89:15:165] * RF_Track.ns; % mm/c
indexx = ceil(tplot /dt) +1; 
str_label = sprintf('t = %5.2f ns \n', tplot(i) / RF_Track.ns);
plot(zplot,G_unloaded(:,indexx(1)) ,'b','LineWidth',3, 'displayname', str_label) % unloaded
for s = indexx(2:end-1)
    i += 1;  
    hold on
    str_label = sprintf('t = %5.2f ns \n', tplot(i) / RF_Track.ns);
    plot(zplot, G_unloaded(:,s) ,'--r','LineWidth',2, 'displayname', str_label)
endfor
i += 1; 
str_label = sprintf('t = %5.2f ns \n', tplot(i) / RF_Track.ns);
plot(zplot, G_unloaded(:,indexx(end)) , 'r', 'LineWidth',3, 'displayname', str_label)
grid on
xlabel('z [mm]', 'FontSize', 15)
ylabel(' G [MV/m]', 'FontSize', 15)
title('Unloaded gradient', 'FontSize', 15);
h = legend;        set(h, 'FontSize',15,'Location', 'SouthWest');
set(gca, "linewidth", 1, "Fontsize", 15);

subplot(224)
tplot  = [0:15:75] * RF_Track.ns; % mm/c
indexx = ceil(tplot /dt) +1; 

plot(zplot,G(:,1),'b','LineWidth',3) % unloaded
for s = indexx(2:end-1)
    hold on
    plot(zplot, G(:,s),'--r','LineWidth',2) 
endfor
plot(zplot, G(:,end), 'r', 'LineWidth',3)
grid on

xlabel('z [mm]'); ylabel(' G [MV/m]'); 
title('Beam induced field (TRANSIENT)')
h = legend('t = 0.0', 't = 15 ns', 't = 30 ns', 't = 45 ns', 't = 60 ns',  't = 75 ns', 'Location', 'SouthWest' );
set(gca, 'FontSize', 15);
set(h, 'FontSize',15)

figure(2)
i = 1; 
tplot2  = [0:15:75] * RF_Track.ns; % mm/c
indexx2 = ceil(tplot2 /dt) +1; 

tplot = [89:15:165] * RF_Track.ns; % mm/c
indexx = ceil(tplot /dt) +1; 
str_label = sprintf('t = %5.2f ns \n', tplot(i) / RF_Track.ns);
plot(zplot,G_unloaded(:,indexx(1)) + G(:,indexx2(1)) ,'b','LineWidth',3, 'displayname', str_label) % unloaded
for s = indexx(2:end-1)
    i += 1;  
    hold on
    str_label = sprintf('t = %5.2f ns \n', tplot(i) / RF_Track.ns);
    plot(zplot, G_unloaded(:,s) + G(:,indexx2(i)) ,'--r','LineWidth',2, 'displayname', str_label)
endfor
i += 1; 
str_label = sprintf('t = %5.2f ns \n', tplot(i) / RF_Track.ns);
plot(zplot, G_unloaded(:,indexx(end)) + G(:,end) , 'r', 'LineWidth',3, 'displayname', str_label)
grid on
xlabel('z [mm]', 'FontSize', 15)
ylabel(' G [MV/m]', 'FontSize', 15)
title('Superposition gradient' , 'FontSize', 15)
h = legend;
set(h, 'FontSize',15);
set(gca, "linewidth", 1, "Fontsize", 15);

%% TRACKING	
% Define bunch
Nparticles = 1000;
Part.mass = RF_Track.electronmass; % MeV/c2
Part.Q = -1; % e
Part.P = 9e3; % MeV (9 GeV)
Psigma = 0*15; % permil, relative energy spread
population = 3.7e9; % e

Twiss = Bunch6d_twiss();
Twiss.emitt_x = 600e-3; % mm.mrad normalised emittance
Twiss.emitt_y = 10e-3; % mm.mrad
Twiss.beta_x = 10; % m
Twiss.beta_y = 10; % m
Twiss.alpha_x = 0;
Twiss.alpha_y = 0;
Twiss.sigma_t = 0.045; % mm/c
Twiss.sigma_pt = Psigma; % permil, relative energy spread

B0 = Bunch6d(Part.mass, population, Part.Q, Part.P, Twiss, Nparticles, -1);
M0 = B0.get_phase_space();

P0 = Bunch6d(RF_Track.electronmass, charge * RF_Track.C, -1, [ 0 0 0 0 0 9e3 ] );

% BEAM
B       = Beam(B0); 
spacing = RF_Track.s / fb; % mm/c

for i = 1:148
    B.append(P0, spacing); 
endfor

B.append(B0, spacing); 

% Lattice
L = Lattice(); 
TWS.add_collective_effect(BL);
TWS.set_cfx_nsteps(50);
L.append(TWS);

%% Set optimal phase for TW structure
% Define witness bunch 
max_energy = L.autophase(P0);

% Track
tic
B1 = L.track(B); 
toc

figure(3)
clf ; hold on
for bunch = 1:150
    M1 = B1{bunch}.get_phase_space();
    M0 = B{bunch}.get_phase_space();  
    scatter(M1(:,5) / c, M0(:,6),'b')
    scatter(M1(:,5) / c, M1(:,6),'r')
endfor

title('Beam energy after CLIC AS','FontSize',15)
grid on 
xlabel('t [ns]','FontSize',15)
ylabel('E [MeV]', 'FontSize', 15) 
h = legend('Initial Energy','Final Energy','Location','NorthEast');
set(h, 'FontSize',15)
set(gca, 'FontSize',12,'LineWidth',1)

% Tracking and field accuracy
E1   = B1{1}.get_phase_space()(:,6);      % MeV
E150 = B1{150}.get_phase_space()(:,6);    % MeV
Espread_tracking = mean(E1) - mean(E150); % MeV

% Ltotal = BL_power.get_z1 - BL_power.get_z0; % m
% G_unloaded_steady = BL_power.get_G_unloaded_steady() / 1e6; % MV/m 
% G_steady = BL_power.get_G_steady() / 1e6; % MV/m 

% mean_dG_unloaded  = mean(G_unloaded(:,end)) - mean(G_unloaded(:,1)); % MV/m
% mean_dG_beam = mean(G_power(:,end)) - mean(G_power(:,1)); % MV/m
% Espread_fieldmaps = (mean_dG_unloaded + mean_dG_beam) * Ltotal; % MeV

fprintf('BL Energy spread from tracking = %5.2f MeV \n', Espread_tracking); 
% fprintf('BL Energy spread from fieldmap = %5.2f MeV \n', Espread_fieldmaps); 
