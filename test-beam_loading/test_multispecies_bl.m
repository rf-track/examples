clear all; close all; clc; 
RF_Track; 
c = RF_Track.clight / 1e6; % mm/ns - speed of light

%% Define Train
% Define bunch
Nparticles = 1000;
Part.mass  = RF_Track.electronmass; % MeV/c2
Part.P     = 2860; % MeV
Psigma     = 0.00 * Part.P; % MeV/c
gamma      = Part.P / Part.mass; 
aver_I     = 101; % A
fb         = 11.994e9; % Hz
population = aver_I * RF_Track.C / fb; % e - 1 A/Hz = * 6.2415091e+18 e

Twiss          = Bunch6d_twiss();
Twiss.emitt_x  = 600e-3; % mm.mrad normalised emittance
Twiss.emitt_y  = 10e-3; % mm.mrad
Twiss.beta_x   = 1.25; % m
Twiss.beta_y   = 1.25; % m
Twiss.alpha_x  = 0;
Twiss.alpha_y  = 0;
Twiss.sigma_t  = 0.3; % mm/c, bunch length
Twiss.sigma_pt = 0; % permil

B0_pos = Bunch6d(Part.mass, population, +1, Part.P, Twiss, Nparticles, -1);
B0_e   = Bunch6d(Part.mass, population, -1, Part.P, Twiss, Nparticles, -1);

% Define beam
Nbunches = 10;
spacing = RF_Track.s / fb ; % mm/c

B = Beam();
for i = 1:Nbunches; % (Nbunches - 1);
    if (mod(i,2) == 0)
        B.append(B0_e,   spacing);
    else
        B.append(B0_pos, spacing);
    end
end

%% Transient BeamLoading Effect
f0           = fb; % Hz
VG           = 0.453*0; % c
QQ           = 72000;
r_Q          = 2294; %  * 1e11; % Ohm/m
phaseadvance = pi; % rad
Ltotal       = 212.45; % mm
Ncells       = 34;
w            = 2 * pi * fb / RF_Track.clight / 1000; % c/mm 
ffactor      = exp(-w*w*Twiss.sigma_t^2/2); 

tic
BL = BeamLoading(Ncells, f0, phaseadvance, QQ, r_Q, VG);
toc


w = BL.get_wake_function(); % V/pC/m
hz = BL.get_dt(); % mm/c
Lcell = BL.get_Lcell(); % m

Ns = length(w);
s_axis = linspace(0, (Ns -1)*hz, Ns); % mm

f1 = figure(1);
plot(s_axis / 1e6, w, 'LineWidth', 2);
xlabel('t [km/c]'); ylabel('w_l [V/pC/m]');
set(gca, 'FontSize', 15);

%% TRACKING 
PETS = TimeDependent_Field ( Ltotal / 1000 );
PETS.add_collective_effect(BL);
PETS.set_cfx_nsteps(Ncells);

%% Lattice - Concatenation of several PETS
Npets = 10; 
L = Lattice();
for s = 1:Npets  
    L.append(PETS);
end

% Autophase
P0 = Bunch6d(RF_Track.electronmass, population, -1, [ 0 0 0 0 0 2400 ] );
max_energy = L.autophase(P0)
fprintf('Autophase finished \n'); 

% perform tracking
tic
B1 = L.track(B);
toc 

%% PLOTS
c = RF_Track.clight / 1e6; % mm/ns - speed of light

% a) Beam Longitudinal Phase Space

figure(2); clf ; hold on
figure(3); clf ; hold on
for bunch = 1:Nbunches

    figure(2)
    M1 = B1{bunch}.get_phase_space();
    M0 = B{bunch}.get_phase_space();
    scatter(M1(:,5) / c, M0(:,6)/1e3,'b')
    scatter(M1(:,5) / c, M1(:,6)/1e3,'r')

    figure(3)
    plot(mean(M1(:,5)) / c, mean(M0(:,6))/1e3,'b', 'MarkerSize', 15)
    plot(mean(M1(:,5)) / c, mean(M1(:,6))/1e3, 'r','MarkerSize', 15)
    title('mean long range')
endfor

figure(2)
str_title = sprintf('Beam Energy after %5.2f PETS', Npets); 
title(str_title,'FontSize',15)
grid on
xlabel('t [ns]','FontSize',15)
ylabel('E [GeV]', 'FontSize', 15)
h = legend('Initial Energy','Final Energy','Location','NorthEast');
set(h, 'FontSize',15)
set(gca, 'FontSize',12,'LineWidth',1)

figure(3)
str_title = sprintf('Beam Mean Energy after %5.2f PETS', Npets);
title(str_title,'FontSize',15)
grid on
xlabel('t [ns]','FontSize',15)
ylabel('<E> [GeV]', 'FontSize', 15)
h = legend('Initial Energy','Final Energy','Location','NorthEast');
set(h, 'FontSize',15)
set(gca, 'FontSize',12,'LineWidth',1)
