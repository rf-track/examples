
function Ez_new = scale_fieldmap (Ez_orig,Delta_cell_1,Delta_cell_2,Delta_cell_3,Delta_cell_4)
% Ez_orig is the original on axis EZ field in Img and Reel components (txt file: pos, re, im)
% N is the number of cells in the accelerator
% Delta_cell_1 = Change in distance in mm for the first cell
% Delta_cell_2 = ...second cell
% Delta_cell_3 = ...third cell
% Delta_cell_4 = ...fourth cell

f= 11.994e9;             % X-band frequency
c = 299792458;           % speed of light
phase_advance = 120;     %phase advance per cell

L = length(Ez_orig);      %number of simulation points
length_cell = 1000*c*phase_advance/f/360;   %length of one cell in mm
lenght_map = Ez_orig(L,1);      % length of the simulation map in mm

%first_aperture= lenght_map/2- N*length_cell/2;  % position first aperture in mm
%second_aperture = first_aperture +length_cell;  % position second aperture in mm
%third_aperture = first_aperture + 2*length_cell;   % position third aperture in mm
%fourth_aperture = first_aperture + 3*length_cell;  % position fourth aperture in mm
%fifth_aperture = first_aperture + 4*length_cell;  % position fifth aperture in mm

first_aperture= 14;  % position first aperture in mm found in step file
second_aperture = 22.495 ;  % position second aperture in mm found in step file
third_aperture = 30.827;   % position third aperture in mm found in step file
fourth_aperture = 39.159;  % position fourth aperture in mm found in step file
fifth_aperture = 47.491;  % position fifth aperture in mm found in step file

pos_fist_ap = round(first_aperture*10+1); % position first aperture in matrix
pos_second_ap = round(second_aperture*10+1);
pos_third_ap = round(third_aperture*10+1);
pos_fourth_ap = round(fourth_aperture*10+1);
pos_fifth_ap = round(fifth_aperture*10+1);

% Copy original EZ to new EZ before first cell
Ez_new = Ez_orig(1:pos_fist_ap,:);

% Adjsut first cell

for i = pos_fist_ap+1:1:pos_second_ap

    s_str = Ez_orig(i,1)+Delta_cell_1/2*(1-cos(pi*(Ez_orig(i,1)-Ez_orig(pos_fist_ap,1))/length_cell));    % see article PRAB 19, 072001(2016)
    Ez_new (i,1) = s_str; 
    Ez_new (i,2) = Ez_orig(i,2);
    Ez_new (i,3) = Ez_orig(i,3);


end

displacement12 =  Ez_new (pos_second_ap,1)-Ez_orig (pos_second_ap,1);

for i = pos_second_ap+1:1:pos_third_ap
    s_str = Ez_orig(i,1)+ displacement12 + Delta_cell_2/2*(1-cos(pi*(Ez_orig(i,1)-Ez_orig(pos_second_ap,1))/length_cell));    % see article PRAB 19, 072001(2016)
    Ez_new (i,1) = s_str; 
    Ez_new (i,2) = Ez_orig(i,2);
    Ez_new (i,3) = Ez_orig(i,3);

end

displacement23 =  Ez_new (pos_third_ap,1)-Ez_orig (pos_third_ap,1);

for i = pos_third_ap+1:1:pos_fourth_ap 
    s_str = Ez_orig(i,1)+ displacement23 + Delta_cell_3/2*(1-cos(pi*(Ez_orig(i,1)-Ez_orig(pos_third_ap,1))/length_cell));    % see article PRAB 19, 072001(2016)
    Ez_new (i,1) = s_str; 
    Ez_new (i,2) = Ez_orig(i,2);
    Ez_new (i,3) = Ez_orig(i,3);

end

displacement34 =  Ez_new (pos_fourth_ap,1)-Ez_orig (pos_fourth_ap,1);


for i = pos_fourth_ap+1:1:pos_fifth_ap 
    s_str = Ez_orig(i,1)+ displacement34 + Delta_cell_4/2*(1-cos(pi*(Ez_orig(i,1)-Ez_orig(pos_fourth_ap,1))/length_cell));    % see article PRAB 19, 072001(2016)
    Ez_new (i,1) = s_str; 
    Ez_new (i,2) = Ez_orig(i,2);
    Ez_new (i,3) = Ez_orig(i,3);

end

displacement45 =  Ez_new (pos_fifth_ap,1)-Ez_orig (pos_fifth_ap,1);



for i = pos_fifth_ap+1:1:L 
    Ez_new (i,1) = Ez_orig(i,1)+ displacement45; 
    Ez_new (i,2) = Ez_orig(i,2);
    Ez_new (i,3) = Ez_orig(i,3);
end

plot(Ez_new (:,1),Ez_new (:,3));
hold on
plot(Ez_new (:,1),Ez_new (:,2));
 

