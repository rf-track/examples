%% Load RF_Track
RF_Track;

global Ez_orig = load('Ez_on-axis_TD26__vg1p8_r05_CC_EDMS.txt');
Ez_new  = scale_fieldmap (Ez_orig,0,0,0,0);

%% new file (irregular mesh)
Zn = Ez_new(:,1);
En = complex(Ez_new(:,2), Ez_new(:,3));

%% regularize the mesh
Zf_dlt = 0.1; % mm
Zf_N   = floor((Zn(end) - Zn(1)) / Zf_dlt);
Zf_len = Zf_N * Zf_dlt; % mm
Zf_min = Zn(1); % mm
Zf_max = Zn(1) + Zf_len; % mm

Zf = linspace(Zf_min, Zf_max, Zf_N+1);
Ef = interp1 (Zn, En, Zf, "spline");

% figure(1) ; clf ; hold on ; plot (Zn, real(En)); plot (Zf, real(Ef), 'r-');
% figure(2) ; clf ; hold on ; plot (Zn, imag(En)); plot (Zf, imag(Ef), 'r-');
% drawnow

%% create an RF-Track element
RF = RF_FieldMap_1d(Ef, Zf_dlt/1e3, Zf_len/1e3, 11.994e9, +1);
RF.set_P_actual(10e6);
RF.set_P_map(4);
RF.set_t0(0.0);
RF.set_odeint_algorithm("leapfrog");
RF.set_smooth(10);

%%%%%

Ex = Ey = Ez = [];
for z = Zf
    [E,B] = RF.get_field(1,1,z,0);
    Ex = [ Ex ; E(1) ];
    Ey = [ Ey ; E(2) ];
    Ez = [ Ez ; E(3) ];
    B
end

figure(1)
clf
plot(Zf, Ex);
xlabel('S [mm]')
ylabel('Ex [V/m]')

figure(2)
clf
plot(Zf, Ey);
xlabel('S [mm]')
ylabel('Ey [V/m]')

figure(3)
clf
plot(Zf, Ez);
xlabel('S [mm]')
ylabel('Ez [V/m]')
