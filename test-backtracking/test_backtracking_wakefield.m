RF_Track;

%% define wakefield
SRWF = ShortRangeWakefield(0.002, 0.005, 0.006); % a,g,l

D = Drift(1);
D.set_odeint_algorithm('rk2');
D.add_collective_effect(SRWF);
D.set_cfx_nsteps(15);

%% bunch
sigma_t = 0.1; % mm/c
Pspread = 0.001; % permil, momentum spread
Pref = 50; % MeV/c

Twiss = Bunch6d_twiss();
Twiss.sigma_t = sigma_t; % mm/c
Twiss.sigma_pt = Pspread; % permil

B0 = Bunch6d_QR(RF_Track.electronmass, RF_Track.nC, -1, Pref, Twiss, 1000);
M0 = B0.get_phase_space();

%% lattice
L = Lattice();
L.append(D);

%% main loop
figure(1);
clf;
for i=1:4
    subplot(2,2,i); hold on
end

for X = linspace(-1, 1, 15)

    M0(:,1) = X;
    B0.set_phase_space(M0);
    
    %% track
    B1 = L.track(B0); % tracking
    B2 = L.btrack(B1); % backtracking
    
    %% do plots
    M1 = B1.get_phase_space(); % phase space after tracking
    M2 = B2.get_phase_space(); % phase space after backtracking
    
    subplot(2,2,1)
    scatter3(M1(:,5)/RF_Track.ns, M1(:,1), M1(:,2))

    subplot(2,2,2)
    scatter3(M1(:,5)/RF_Track.ns, M1(:,1), M1(:,6))

    subplot(2,2,3)
    scatter3(M2(:,5)/RF_Track.ns, M2(:,1), M2(:,2))

    subplot(2,2,4)
    scatter3(M2(:,5)/RF_Track.ns, M2(:,1), M2(:,6))
    
end

subplot(2,2,1)
title('Tracked bunch')
xlabel('\Delta{t} [ns]');
ylabel('x [mm]');
zlabel('x'' [mrad]');

subplot(2,2,2)
title('Tracked bunch')
xlabel('\Delta{t} [ns]');
ylabel('x [mm]');
zlabel('P [MeV/c]');

subplot(2,2,3)
title('Backtracked initial bunch')
xlabel('\Delta{t} [ns]');
ylabel('x [mm]');
zlabel('x'' [mrad]');

subplot(2,2,4)
title('Backtracked initial bunch')
xlabel('\Delta{t} [ns]');
ylabel('x [mm]');
zlabel('P [MeV/c]');
