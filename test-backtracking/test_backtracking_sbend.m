%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.protonmass; % MeV/c^2
charge = +1; % single-particle charge, in units of e
population = 1e10; % number of real particles per bunch
Pc = 1000; % reference momentum, MeV/c
rho = 10; % m, bending radius
By = Pc * 1e6 / rho  / RF_Track.clight; % T
Brho = Pc / charge; % MV/c, reference rigidity

%% Setup the sector bending magnet
B = SBend(0.5);
B.set_Brho(Brho);
B.set_Bfield(By);
B.set_h(0.0);
B.set_tt_nsteps(100);
B.set_hgap(0.5);
B.set_fint(0.6);
A = B.get_angle()
B.set_E1(A/5);
B.set_E2(A/3);

% Setup the lattice
L = Lattice();
L.append(B);

%% Define Twiss parameters
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 0.001; % mm.mrad, normalized emittances
Twiss.emitt_y = 0.001; % mm.mrad
Twiss.alpha_x = 0.0;
Twiss.alpha_y = 0.0;
Twiss.beta_x = 1; % m
Twiss.beta_y = 1; % m
Twiss.sigma_t = 0.01;
Twiss.sigma_pt = 1;

%% Create the bunch
B0 = Bunch6d(mass, population, charge, Pc, Twiss, 10000);

%% Perform tracking
B1 = L.track(B0);

%% Retrieve the Twiss plot and the phase space
T0 = L.get_transport_table('%S %beta_x %beta_y');
M0 = B0.get_phase_space('%x %xp %y %yp %t %P');

%% backtracking
B2 = L.btrack(B1);
T2 = L.get_transport_table('%S %beta_x %beta_y');
M2 = B2.get_phase_space('%x %xp %y %yp %t %P');

figure(1)
clf
subplot(2,2,1);
hold on
scatter(M0(:,1), M0(:,2), '*');
scatter(M2(:,1), M2(:,2), '.');
legend('initial bunch', 'back tracking');
xlabel('x [mm]');
ylabel('x'' [mrad]');
set(gca, 'linewidth', 2, 'fontsize', 14);
box

subplot(2,2,2);
hold on
scatter(M0(:,3), M0(:,4), '*');
scatter(M2(:,3), M2(:,4), '.');
legend('initial bunch', 'back tracking');
xlabel('y [mm]');
ylabel('y'' [mrad]');
set(gca, 'linewidth', 2, 'fontsize', 14);
box

subplot(2,2,3);
hold on
scatter(M0(:,5), M0(:,6), '*');
scatter(M2(:,5), M2(:,6), '.');
legend('initial bunch', 'back tracking');
xlabel('t [mm/c]');
ylabel('P [MeV/c]');
set(gca, 'linewidth', 2, 'fontsize', 14);
box

figure(2);
clf
hold on
plot(T0(:,1), T0(:,2), 'b-', 'linewidth', 2);
plot(T2(:,1), T2(:,2), 'r-', 'linewidth', 2);
lgnd = legend({ 'tracking ', 'backtracking ' });
xlabel('S [m]');
ylabel('\beta [m]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box