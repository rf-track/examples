%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 1e10; % number of real particles per bunch
Pc = 5; % reference momentum, MeV/c
B_rho = Pc / charge; % MV/c, reference rigidity

%% FODO cell paramters
Lcell = 2; % m
Lquad = 10; % m
Ldrift = Lcell/2 - Lquad; % m
mu = 90; % deg
k1L = sind(mu/2) / (Lcell/4); % 1/m
strength = k1L * B_rho; % MeV/m

%% Define Twiss parameters
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 0.001; % mm.mrad, normalized emittances
Twiss.emitt_y = 0.001; % mm.mrad
Twiss.alpha_x = 0.0;
Twiss.alpha_y = 0.0;
Twiss.beta_x = Lcell * (1 + sind(mu/2)) / sind(mu); % m
Twiss.beta_y = Lcell * (1 - sind(mu/2)) / sind(mu); % m
Twiss.sigma_t = 0.01;
Twiss.sigma_pt = 1;

%% Create the bunch
B0 = Bunch6d(mass, population, charge, Pc, Twiss, 1000);


% Setup the elements
Qf = Quadrupole(Lquad/2, strength/2);
Qf.set_tt_nsteps(100);

QD = Quadrupole(Lquad, -strength);
Dr = Drift(Ldrift);
Dr.set_tt_nsteps(100);
Dr.set_odeint_algorithm('rk2');

for i=1:2

    % Setup the lattice
    FODO = Lattice();
    %FODO.append(Qf);
    FODO.append(Dr);
    %FODO.append(QD);
    %FODO.append(Dr);
    %FODO.append(Qf);

    if i==2
        FODO{1}.set_offsets(0.5, 0.2, 0, 0.2, 0.1, 0.2, 'center');
    end

    %% Perform tracking
    B1 = FODO.track(B0);

    %% Retrieve the Twiss plot and the phase space
    T0 = FODO.get_transport_table('%S %beta_x %beta_y');
    M0 = B0.get_phase_space('%x %xp %y %yp %t %P');

    %% backtracking
    B2 = FODO.btrack(B1);
    T2 = FODO.get_transport_table('%S %beta_x %beta_y');
    M2 = B2.get_phase_space('%x %xp %y %yp %t %P');

    %% Plots
    figure(1)
    clf
    subplot(2,2,1);
    hold on
    scatter(M0(:,1), M0(:,2), '*');
    scatter(M2(:,1), M2(:,2), '.');
    legend('initial bunch', 'back tracking');
    xlabel('x [mm]');
    ylabel('x'' [mrad]');
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box

    subplot(2,2,2);
    hold on
    scatter(M0(:,3), M0(:,4), '*');
    scatter(M2(:,3), M2(:,4), '.');
    legend('initial bunch', 'back tracking');
    xlabel('y [mm]');
    ylabel('y'' [mrad]');
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box

    subplot(2,2,3);
    hold on
    scatter(M0(:,5), M0(:,6), '*');
    scatter(M2(:,5), M2(:,6), '.');
    legend('initial bunch', 'back tracking');
    xlabel('t [mm/c]');
    ylabel('P [MeV/c]');
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box

    figure(2)
    if i==1
        clf
    end 
    M1 = B1.get_phase_space('%x %xp %y %yp %t %P');
    subplot(2,2,1);
    hold on
    scatter(M1(:,1), M1(:,2));
    xlabel('x [mm]');
    ylabel('x'' [mrad]');
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box

    subplot(2,2,2);
    hold on
    scatter(M1(:,3), M1(:,4));
    xlabel('y [mm]');
    ylabel('y'' [mrad]');
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box

    subplot(2,2,3);
    hold on
    scatter(M1(:,5), M1(:,6));
    xlabel('t [mm/c]');
    ylabel('P [MeV/c]');
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box
end
