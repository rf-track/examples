RF_Track;

load AMD_field_2d.dat.gz;

%% create RF-Track element

AMD = Static_Magnetic_FieldMap_2d_LINT(Br, Bz, hr/1e3, hz/1e3);

%% make plots

Raxis = ((1:size(Bz,2))-1)*hr; % mm
Zaxis = ((1:size(Bz,1))-1)*hz; % mm

Bz_ = [];
for Z = Zaxis
    [E_,B_] = AMD.get_field(Raxis(1), 0, Z, 0);
    Bz_ = [ Bz_ ; B_(3) ];
end

figure(1)
clf
plot(Zaxis, Bz(:,1), 'r*');
hold on
plot(Zaxis, Bz_);
xlabel('S [mm]');
ylabel('Bz [T]');

figure(2)
plot(Zaxis, Bz(:,1) - Bz_)
xlabel('S [mm]');
ylabel('Bz_{interp} - Bz_{field map} [T]');
