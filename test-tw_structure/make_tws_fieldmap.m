RF_Track;

TWS = make_tws(1.0, 0);
V = Volume();
V.add(TWS, 0, 0, 0);
S_axis = linspace(0, TWS.get_length(), 500);

Ez_0 = [];
for S = S_axis*1e3
    [E,B] = V.get_field(0,0,S,0);
    Ez_0 = [ Ez_0 ; E(3) ];
end

TWS = make_tws(1.0, -90);
V = Volume();
V.add(TWS, 0, 0, 0);

Ez_90 = [];
for S = S_axis*1e3
    [E,B] = V.get_field(0,0,S,0);
    Ez_90 = [ Ez_90 ; E(3) ];
end

T = [ S_axis' Ez_0 Ez_90 ];

fid = fopen('fields/TWS_RFT.dat', 'w');
fprintf(fid, '%g %g %g\n', T');
fclose(fid);
