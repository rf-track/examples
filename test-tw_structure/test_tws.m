RF_Track;

phid = 90;
maxE = 1; % V/m

TWS = make_tws(maxE, phid);

V = Volume();
V.add(TWS, 0, 0, 0);

%% make plots
freq = TWS{1}.get_frequency(); % Hz
T_period = RF_Track.clight / freq * 1e3; % 1 / 2.856 mm/c

S_axis = linspace(0, V.get_length()*1e3, 40); % mm
T_axis = linspace(0, T_period, 32)(1:end-1); % mm/c

T = load('fields/TWS_RFT.dat');

Ez = complex(T(:,2), T(:,3));
hz = T(2,1)-T(1,1);

RF = RF_FieldMap_1d_CINT(Ez, hz, -1, freq, +1);
RF.set_phid(phid);
RF.set_t0(0.0);

figure(1);
title('E_z');

t0 = 0;
t_ = 0;
for n=1:3
    for t = T_axis
        Ez_tws = [];
        Bx_tws = [];
        Ez_rf  = [];
        Bx_rf  = [];
        for s = S_axis
            [E,B] = TWS.get_field(0, 5, s, t0 + t_ + t); 
            Ez_tws = [ Ez_tws ; E(3) ];
            Bx_tws = [ Bx_tws ; B(1) ];
            [E,B] = RF.get_field (0, 5, s, t0 + t_ + t);
            Ez_rf =  [ Ez_rf ; E(3) ];
            Bx_rf =  [ Bx_rf ; B(1) ];
        end
        subplot(2,1,1);
        plot(S_axis, Ez_tws, 'b-', S_axis, Ez_rf, 'r*');
        legend('Analytic SW+TW', 'RF\_FieldMap\_1d')
        axis([ 0 S_axis(end) -1 1 ]);
        ylabel('E_z (V/m)')
        subplot(2,1,2);
        plot(S_axis, Bx_tws, 'b-', S_axis, Bx_rf, 'r*');
        legend('Analytic SW+TW', 'RF\_FieldMap\_1d')
        axis([ 0 S_axis(end) -1e-12 1e-12 ]);
        ylabel('B_x (T)')
        drawnow;

    end
    t_ += T_period;
end